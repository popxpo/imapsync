# Maintainer: Frederik Schwan <freswa at archlinux dot org>

pkgname=imapsync
pkgver=2.229
pkgrel=1
pkgdesc='IMAP synchronisation, sync, copy or migration tool'
arch=('any')
url='https://github.com/imapsync/imapsync'
license=('custom:imapsync')
depends=(perl-{cgi,crypt-openssl-rsa,data-uniqid,date-manip,encode-imaputf7,file-copy-recursive}
  perl-{file-tail,html-parser,io-socket-inet6,io-socket-ssl,io-tee,json-webtoken,libwww}
  perl-{module-scandeps,mail-imapclient,module-runtime,ntlm,package-stash,proc-processtable}
  perl-{readonly,regexp-common,sys-meminfo,term-readkey,unicode-string}
)
makedepends=(cpanminus perl-par-packer)
checkdepends=(inetutils lsb-release time procps-ng
  perl-{test-deep,test-fatal,test-mock-guard,test-mockobject,test-pod,test-requires,test-warn}
)
source=("https://github.com/imapsync/imapsync/archive/refs/tags/${pkgname}-${pkgver}.tar.gz"
        "fix-test.patch")
b2sums=('1fbb52e014e18ed8970e56031b64aa3429d6383bc71b0d47e08528b300dfda4591ba279e6c752bdf3a609ae6873bddb61cdcc8ead53750112fc1969e101e1a99'
        '9fda3853af3fc5a8977296bb28b030e363ec32a570fd02e7736d872c8959fdfb9cc098d20bcebcc7ef26c83531ef83cb37b5459ccaecc404d3ee9006c8fb30d4')

prepare() {
  cd ${pkgname}-${pkgname}-${pkgver}
  patch -Np1 -i ../fix-test.patch
}

build() {
  make -C ${pkgname}-${pkgname}-${pkgver}
}

check() {
  cd ${pkgname}-${pkgname}-${pkgver}
  perl imapsync --tests
}

package() {
  cd ${pkgname}-${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -Dm644 LICENSE "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE.txt
}
